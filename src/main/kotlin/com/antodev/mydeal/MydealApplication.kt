package com.antodev.mydeal

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

@SpringBootApplication
class MydealApplication

fun main(args: Array<String>) {
    SpringApplication.run(MydealApplication::class.java, *args)
}
