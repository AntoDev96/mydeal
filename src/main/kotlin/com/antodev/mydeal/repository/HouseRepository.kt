package com.antodev.mydeal.repository

import com.antodev.mydeal.model.House
import com.antodev.mydeal.model.SellCategory
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.JpaRepository

interface HouseRepository:JpaRepository<House, Long> {
    fun findBySellCategory(sellCategory: SellCategory, page: Pageable): Page<House>
}