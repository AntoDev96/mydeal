package com.antodev.mydeal.repository

import com.antodev.mydeal.model.User
import org.springframework.data.jpa.repository.JpaRepository

interface UserRepository: JpaRepository<User, Long>  {
    fun findOneByName(username: String): User
}