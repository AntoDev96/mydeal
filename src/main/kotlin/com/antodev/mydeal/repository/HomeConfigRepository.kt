package com.antodev.mydeal.repository

import com.antodev.mydeal.model.HomeConfig
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query

interface HomeConfigRepository:JpaRepository<HomeConfig, Long> {

    @Query("from HomeConfig order by id asc")
    fun findFirst(): HomeConfig?
}