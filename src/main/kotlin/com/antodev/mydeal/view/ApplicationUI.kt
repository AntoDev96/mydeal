package com.antodev.mydeal.view

import com.antodev.mydeal.view.page.component.SearchBar
import com.ejt.vaadin.sizereporter.SizeReporter
import com.vaadin.annotations.Theme
import com.vaadin.annotations.Title
import com.vaadin.annotations.Widgetset
import com.vaadin.navigator.View
import com.vaadin.navigator.ViewDisplay
import com.vaadin.server.Sizeable
import com.vaadin.server.VaadinRequest
import com.vaadin.shared.ui.MarginInfo
import com.vaadin.spring.annotation.SpringUI
import com.vaadin.spring.annotation.SpringView
import com.vaadin.spring.annotation.SpringViewDisplay
import com.vaadin.ui.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.ApplicationContext
import com.vaadin.ui.themes.ValoTheme
import javax.swing.plaf.basic.BasicSliderUI


@SpringUI(path = "/")
@Theme("mydeal")
@Title("My Deal")
@SpringViewDisplay
@Widgetset("com.antodev.mydeal.MyDealWidgetSet")
class ApplicationUI: UI(), ViewDisplay {
    @Autowired
    lateinit var searchBar: SearchBar

    @Autowired
    lateinit var context: ApplicationContext

    var pageViewer: Panel = Panel()

    override fun showView(view: View?) {
        if(view == null) {
            Notification.show("Questa pagina non esiste!")
            return
        }
        pageViewer.content = view as Component
    }

    override fun init(request: VaadinRequest?) {
        navigator.navigateTo("home")
        val title = createNavigationButton("My Deal", "")
        val navbarButtons = HorizontalLayout()
        navbarButtons.defaultComponentAlignment = Alignment.MIDDLE_LEFT
        val navbar = HorizontalLayout(title, navbarButtons as Component)
        navbar.isSpacing = true
        navbar.margin = MarginInfo(true, true)
        navbar.defaultComponentAlignment = Alignment.BOTTOM_LEFT
        navbar.setExpandRatio(title, 1.0f)
        navbar.setStyleName(CustomComponents.NAVBAR.cssSelector)
        navbar.addStyleName(ValoTheme.MENUBAR_BORDERLESS)
        navbar.setWidth(100f, Sizeable.Unit.PERCENTAGE)
        val views = context.getBeansWithAnnotation(MenuItem::class.java)

        views.toSortedMap(Comparator(function = fun(first, second):Int {
            val firstMenuItem = (views[first] as View).javaClass.annotations.find { annotation -> annotation is MenuItem } as MenuItem
            val secondMenuItem = (views[second] as View).javaClass.annotations.find { annotation -> annotation is MenuItem } as MenuItem
            return firstMenuItem.position - secondMenuItem.position
        })).values.forEach(fun(view) {
            val menuItem = (view as View).javaClass.annotations.find { annotation -> annotation is MenuItem } as MenuItem
            val springView = (view as View).javaClass.annotations.find { annotation -> annotation is SpringView } as SpringView
            val button = createNavigationButton(menuItem.name, springView.name)
            navbarButtons.addComponent(button)
        })

        pageViewer.setStyleName(ValoTheme.PANEL_BORDERLESS)

        val footer = HorizontalLayout()
        footer.addStyleName(CustomComponents.FOOTER.cssSelector)
        footer.setWidth(100.0f, Sizeable.Unit.PERCENTAGE)
        footer.margin = MarginInfo(true)

        val rootLayout = VerticalLayout(navbar, pageViewer, footer)
        rootLayout.setExpandRatio(pageViewer, 1.0f)
        rootLayout.setWidth(100.0f, Sizeable.Unit.PERCENTAGE)
        rootLayout.isSpacing = false
        rootLayout.margin = MarginInfo(false)

        content = rootLayout

        ui.addWindow(searchBar)
        searchBar.focus()

    }

    private fun createNavigationButton(caption: String, viewName: String): Component {
        val button = Button(caption)
        button.styleName = ValoTheme.BUTTON_BORDERLESS
        button.addClickListener {_ -> ui.navigator.navigateTo(viewName)}
        return button
    }

}