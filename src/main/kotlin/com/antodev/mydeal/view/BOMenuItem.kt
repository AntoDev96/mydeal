package com.antodev.mydeal.view

annotation class BOMenuItem(val name: String, val position: Int)