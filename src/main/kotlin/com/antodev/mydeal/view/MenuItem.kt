package com.antodev.mydeal.view

annotation class MenuItem(val name: String, val position: Int)