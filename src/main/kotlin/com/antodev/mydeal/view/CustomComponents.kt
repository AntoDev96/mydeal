package com.antodev.mydeal.view

enum class CustomComponents(val cssSelector: String) {
    NAVBAR("MyDealNavbar"),
    SEARCHBAR("MyDealSearchbar"),
    FOOTER("MyDealFooter")
}