package com.antodev.mydeal.view

enum class ViewMenu(val title: String) {
    HOME("Home"),
    FLAT_EVALUATION("Valutiamo il tuo immobile"),
    DESIGN("Progettazione & Disign"),
    ENERGY_CERTIFICATION("Certificazione energetica"),
    FUNDING("Finanziamenti/Mutui"),
    INSURANCE("Assicurazioni")
}