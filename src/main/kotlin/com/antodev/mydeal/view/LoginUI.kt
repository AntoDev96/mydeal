package com.antodev.mydeal.view

import com.antodev.mydeal.view.page.component.SearchBar
import com.ejt.vaadin.sizereporter.SizeReporter
import com.vaadin.annotations.Theme
import com.vaadin.annotations.Title
import com.vaadin.annotations.Widgetset
import com.vaadin.navigator.View
import com.vaadin.navigator.ViewChangeListener
import com.vaadin.navigator.ViewDisplay
import com.vaadin.server.Sizeable
import com.vaadin.server.VaadinRequest
import com.vaadin.shared.ui.MarginInfo
import com.vaadin.spring.annotation.SpringUI
import com.vaadin.spring.annotation.SpringView
import com.vaadin.spring.annotation.SpringViewDisplay
import com.vaadin.ui.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.ApplicationContext
import com.vaadin.ui.themes.ValoTheme
import javax.swing.plaf.basic.BasicSliderUI
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.authentication.dao.DaoAuthenticationProvider
import java.security.AuthProvider


@SpringUI(path = "login")
@Theme("mydeal")
@Title("My Deal")
@SpringView
@Widgetset("com.antodev.mydeal.MyDealWidgetSet")
class LoginUI: UI(), View {
    override fun enter(p0: ViewChangeListener.ViewChangeEvent?) {

    }

    @Autowired
    lateinit var authProvider: DaoAuthenticationProvider

    override fun init(request: VaadinRequest?) {
        val title = Label("My Deal")

        val usernameInput = TextField("Username")
        val passwordInput = PasswordField("Password")
        val loginButton = Button("Login", fun(_) {
            val auth = UsernamePasswordAuthenticationToken(usernameInput.value, passwordInput.value)
            val authenticated = authProvider.authenticate(auth)
            SecurityContextHolder.getContext().authentication = authenticated

            page.setLocation("/admin")
        })

        val loginForm = FormLayout(usernameInput, passwordInput, loginButton)
        content = VerticalLayout(title, loginForm)
    }



}