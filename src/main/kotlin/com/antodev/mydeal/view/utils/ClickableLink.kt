package com.antodev.mydeal.view.utils

import com.vaadin.event.ContextClickEvent
import com.vaadin.event.LayoutEvents
import com.vaadin.server.ExternalResource
import com.vaadin.server.Resource
import com.vaadin.ui.*
import org.springframework.web.servlet.view.InternalResourceView
import java.util.function.Consumer
import java.util.logging.Logger
import javax.annotation.PostConstruct

class ClickableLink(val title:String, val viewName: String): Button(title, fun(_) {

}) {
    val LOG = Logger.getLogger(javaClass.name)
}