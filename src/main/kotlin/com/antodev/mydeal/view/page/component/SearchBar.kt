package com.antodev.mydeal.view.page.component

import com.antodev.mydeal.model.House
import com.antodev.mydeal.model.SellCategory
import com.antodev.mydeal.model.Type
import com.antodev.mydeal.repository.HouseRepository
import com.antodev.mydeal.view.CustomComponents
import com.ejt.vaadin.sizereporter.SizeReporter
import com.vaadin.server.Sizeable
import com.vaadin.spring.annotation.SpringComponent
import com.vaadin.spring.annotation.UIScope
import com.vaadin.spring.annotation.ViewScope
import com.vaadin.ui.*
import com.vaadin.ui.themes.ValoTheme
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Scope
import org.springframework.data.domain.Example
import org.springframework.data.domain.PageRequest
import java.util.*
import java.util.logging.Logger
import javax.annotation.PostConstruct
import javax.persistence.EntityManager
import kotlin.reflect.jvm.internal.impl.load.kotlin.JvmType

@UIScope
@SpringComponent
class SearchBar:Window() {
    val LOG = Logger.getLogger(javaClass.name)
    lateinit var rootLayout: VerticalLayout

    @Autowired
    lateinit var houseRepo: HouseRepository

    @PostConstruct
    fun open() {
        addStyleName(CustomComponents.SEARCHBAR.cssSelector)

        val sellCategoriesComboBox = ComboBox()
        sellCategoriesComboBox.inputPrompt = "Ricerca"
        SellCategory.values().forEach(fun(item) {
            sellCategoriesComboBox.addItem(item)
            sellCategoriesComboBox.setItemCaption(item, item.description)
        })
        val typeComboBox = ComboBox()
        typeComboBox.inputPrompt = "Tipologia"
        val houseCategoryComboBox = ComboBox()
        houseCategoryComboBox.inputPrompt = "Categoria"
        val cityComboBox = ComboBox()
        cityComboBox.inputPrompt = "Città"
        val maxPriceComboBox = ComboBox()
        maxPriceComboBox.inputPrompt = "Prezzo max"
        val firstRow = HorizontalLayout(sellCategoriesComboBox, typeComboBox, houseCategoryComboBox, cityComboBox, maxPriceComboBox)
        val secondRow = HorizontalLayout()
        secondRow.defaultComponentAlignment = Alignment.MIDDLE_LEFT
        secondRow.setWidth(100.0f, Sizeable.Unit.PERCENTAGE)
        rootLayout = VerticalLayout(firstRow, secondRow)
        val thirdRow = HorizontalLayout()
        thirdRow.defaultComponentAlignment = Alignment.MIDDLE_LEFT
        val fourthRow = HorizontalLayout()
        val rifCodeTextBox = TextField()
        rifCodeTextBox.inputPrompt = "RIF.Code"
        rifCodeTextBox.setWidth(100.0f, Sizeable.Unit.PERCENTAGE)
        secondRow.addComponent(rifCodeTextBox)
        secondRow.setExpandRatio(rifCodeTextBox, 1.0f)
        val expandSearch = CheckBox("Espandi ricerca")
        secondRow.addComponent(expandSearch)
        val searchButton = Button("Cerca")
        searchButton.addStyleName(ValoTheme.BUTTON_FRIENDLY)
        secondRow.addComponent(searchButton)

        val statusComboBox = ComboBox()
        statusComboBox.inputPrompt = "Stato"
        thirdRow.addComponent(statusComboBox)
        val areaComboBox = ComboBox()
        areaComboBox.inputPrompt = "Superficie (mq)"
        thirdRow.addComponent(areaComboBox)
        val roomsComboBox = ComboBox()
        roomsComboBox.inputPrompt = "Locali"
        thirdRow.addComponent(roomsComboBox)
        val toiletsComboBox = ComboBox()
        toiletsComboBox.inputPrompt = "Bagni"
        thirdRow.addComponent(toiletsComboBox)
        val otherServicesCheckBox = CheckBox("Altri servizi")
        thirdRow.addComponent(otherServicesCheckBox)
        otherServicesCheckBox.addValueChangeListener(fun(_) {
            if(otherServicesCheckBox.value) {
                rootLayout.addComponent(fourthRow)
            } else {
                rootLayout.removeComponent(fourthRow)
            }
        })
        expandSearch.addValueChangeListener(fun(_) {
            if(expandSearch.value) {
                rootLayout.addComponent(thirdRow)
            } else {
                otherServicesCheckBox.value = false
                rootLayout.removeComponent(thirdRow)
                rootLayout.removeComponent(fourthRow)
            }
        })

        val isFurnishedCheckBox = CheckBox("Arredato")
        val hasGarageCheckBox = CheckBox("Garage/Posto auto")
        val hasGardenCheckBox = CheckBox("Giardino/Cortile")
        val hasSwimmingPoolCheckBox = CheckBox("Piscina")
        val hasInternetCheckBox = CheckBox("Internet")
        val hasFireplaceCheckBox = CheckBox("Canna fumaria/Caminetto")
        fourthRow.addComponents(isFurnishedCheckBox, hasGarageCheckBox, hasGardenCheckBox, hasSwimmingPoolCheckBox, hasInternetCheckBox, hasFireplaceCheckBox)

        val mainPanel = Panel(rootLayout)
        mainPanel.addStyleName(ValoTheme.PANEL_WELL)

        content = mainPanel
        addStyleName(ValoTheme.WINDOW_BOTTOM_TOOLBAR)

        isClosable = false
        isDraggable = true
        isResizable = false

        firstRow.isSpacing = true
        secondRow.isSpacing = true
        thirdRow.isSpacing = true
        fourthRow.isSpacing = true
        rootLayout.isSpacing = true
        rootLayout.setMargin(true)

        searchButton.addClickListener(fun(_) {
            val example = House()
            sellCategoriesComboBox.value?.let { value -> example.sellCategory = value as SellCategory }
            houseRepo.findAll(Example.of(example), PageRequest(0, 10)).forEach({item -> println(item)})
        })

    }
}