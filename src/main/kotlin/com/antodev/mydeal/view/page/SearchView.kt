package com.antodev.mydeal.view.page

import com.antodev.mydeal.view.ApplicationUI
import com.antodev.mydeal.view.MenuItem
import com.vaadin.navigator.View
import com.vaadin.navigator.ViewChangeListener
import com.vaadin.spring.annotation.SpringView
import com.vaadin.spring.annotation.UIScope
import com.vaadin.ui.Label
import com.vaadin.ui.VerticalLayout

@UIScope
@SpringView(name = "search", ui = arrayOf(ApplicationUI::class))
@MenuItem(name = "Search", position = 1)
class SearchView: View, VerticalLayout() {
    override fun enter(event: ViewChangeListener.ViewChangeEvent?) {
        addComponent(Label("Search"))
    }
}