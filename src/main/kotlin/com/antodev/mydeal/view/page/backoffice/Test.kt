package com.antodev.mydeal.view.page.backoffice

import com.antodev.mydeal.view.BOMenuItem
import com.antodev.mydeal.view.BackofficeUI
import com.vaadin.navigator.View
import com.vaadin.navigator.ViewChangeListener
import com.vaadin.spring.annotation.SpringView
import com.vaadin.spring.annotation.UIScope
import com.vaadin.ui.VerticalLayout

@UIScope
@SpringView(name = "test", ui = arrayOf(BackofficeUI::class))
@BOMenuItem(name = "Test", position = 0)
class Test : View, VerticalLayout() {
    override fun enter(p0: ViewChangeListener.ViewChangeEvent?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

}