package com.antodev.mydeal.view.page

import com.antodev.mydeal.model.SellCategory
import com.antodev.mydeal.repository.HomeConfigRepository
import com.antodev.mydeal.repository.HouseRepository
import com.antodev.mydeal.view.ApplicationUI
import com.antodev.mydeal.view.MenuItem
import com.vaadin.navigator.View
import com.vaadin.navigator.ViewChangeListener
import com.vaadin.server.ExternalResource
import com.vaadin.server.Sizeable
import com.vaadin.shared.ui.MarginInfo
import com.vaadin.shared.ui.label.ContentMode
import com.vaadin.spring.annotation.SpringView
import com.vaadin.spring.annotation.UIScope
import com.vaadin.ui.*
import com.vaadin.ui.themes.ValoTheme
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.PageRequest
import org.vaadin.virkki.carousel.HorizontalCarousel
import java.util.logging.Logger
import javax.annotation.PostConstruct

@UIScope
@SpringView(name = "home", ui = arrayOf(ApplicationUI::class))
@MenuItem(name = "Home", position = 0)
class HomeView: View, VerticalLayout() {
    val LOG = Logger.getLogger(javaClass.name)

    @Autowired
    lateinit var houseRepo: HouseRepository

    @Autowired
    lateinit var homeConfigRepo: HomeConfigRepository

    override fun enter(event: ViewChangeListener.ViewChangeEvent?) {}

    @PostConstruct
    fun init() {
        val root = VerticalLayout()
        root.setMargin(false)
        //Image slider
        homeConfigRepo.findFirst()?.let(fun(config) {
            if(config.images.isEmpty()) {
                return
            }
            val carousel = HorizontalCarousel()
            carousel.setWidth(100.0f, Sizeable.Unit.PERCENTAGE)
            carousel.setHeight(700.0f, Sizeable.Unit.PIXELS)
            config.images.forEach(fun(image) {
                val image = Image(null, ExternalResource(image))
                val layout = HorizontalLayout(image)
                layout.setComponentAlignment(image, Alignment.MIDDLE_CENTER)
                layout.setExpandRatio(image, 1.0f)
                layout.setSizeFull()
                carousel.addComponent(layout)
            })
            root.addComponent(carousel)
        })
        //Promotions
        SellCategory.values().forEach(fun(category) {
            val header = Label("<h3>Ultimi aggiunti in ${category.description}</h3>", ContentMode.HTML)
            val headerLayout = HorizontalLayout(header)
            headerLayout.margin = MarginInfo(true, true, false, true)
            val content = HorizontalLayout()
            content.margin = MarginInfo(true)
            content.isSpacing = true
            content.setSizeUndefined()
            houseRepo.findBySellCategory(category, PageRequest(0, 10)).content.forEach(fun(house) {
                val title = Label("<h3>${house.name}</h3>", ContentMode.HTML)
                title.setWidth(350.0f, Sizeable.Unit.PIXELS)
                val titleLayout = HorizontalLayout(title)
                titleLayout.margin = MarginInfo(true)
                val description = Label(house.description)
                description.setWidth(350.0f, Sizeable.Unit.PIXELS)
                val descriptionLayout = HorizontalLayout(description)
                descriptionLayout.setMargin(true)
                val cardContainer = VerticalLayout(titleLayout, descriptionLayout)
                cardContainer.addStyleName(ValoTheme.LAYOUT_CARD)
                content.addComponent(cardContainer)
            })
            val panel = Panel(content)
            panel.styleName = ValoTheme.PANEL_BORDERLESS
            panel.addStyleName(ValoTheme.PANEL_SCROLL_INDICATOR)
            root.addComponents(headerLayout, panel)
        })
        addComponent(root)
    }

}