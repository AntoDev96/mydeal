package com.antodev.mydeal.model

import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.userdetails.UserDetails
import javax.persistence.*

@Entity
class User: UserDetails {
    @Id
    @GeneratedValue
    var id: Long = 0

    @Column(unique = true, nullable = false, name = "name")
    lateinit var name: String

    @Column(name = "password")
    var _password: String = ""

    @ManyToMany
    lateinit var roles: List<Role>

    override fun getUsername(): String {
        return name
    }

    override fun getPassword(): String {
        return _password
    }

    fun setPassword(value: String) {
        _password = value
    }

    override fun getAuthorities(): MutableCollection<out GrantedAuthority> {
        return roles.map { role -> GrantedAuthority({role.name}) }.toMutableList()
    }

    override fun isEnabled(): Boolean {
        return true
    }

    override fun isCredentialsNonExpired(): Boolean {
        return true
    }

    override fun isAccountNonExpired(): Boolean {
        return true
    }

    override fun isAccountNonLocked(): Boolean {
        return true
    }
}