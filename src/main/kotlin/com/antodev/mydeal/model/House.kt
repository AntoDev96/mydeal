package com.antodev.mydeal.model

import javax.persistence.*

@Entity
class House {
    @Id
    @GeneratedValue
    var id: Long = 0

    @Column
    var name: String? = null

    @Column
    var description: String? = null

    @Column
    @Enumerated(EnumType.STRING)
    var sellCategory: SellCategory? = null

    @Column
    @Enumerated(EnumType.STRING)
    var houseCategory: HouseCategory? = null

    @Column
    @Enumerated(EnumType.STRING)
    var type: Type = Type.RESIDENTIAL

    override fun toString(): String {
        return "House(id=$id, name='$name', description=$description, sellCategory=$sellCategory, houseCategory=$houseCategory, type=$type)"
    }


}