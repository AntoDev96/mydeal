package com.antodev.mydeal.model

import org.apache.commons.lang3.StringUtils
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id

@Entity
class HomeConfig {
    @Id
    @GeneratedValue
    var id: Long = 0

    @Column
    private lateinit var _images: String
    var images: List<String>
    get() = _images.split(',')
    set(value) {
        _images = StringUtils.join(value)
    }


}