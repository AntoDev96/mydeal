package com.antodev.mydeal.model

enum class Type(val description: String) {
    RESIDENTIAL("Residenziale"),
    COMMERCIAL("Commerciale"),
    TERRAIN("Terreni")
}