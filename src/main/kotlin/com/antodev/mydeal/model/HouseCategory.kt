package com.antodev.mydeal.model

enum class HouseCategory(val description: String) {

    FLAT("Casa indipendente"),
    INDEPENDENT("Villa/ Villa a schiera"),
    TRULLO("Trulli"), //HOW THE FUCK AM I SUPPOSED TO TRANSLATE THIS SHIT!!! ONLY WITH U PUGLIESE BITCHES
    LOFT("Loft"),
    FARM("Masseria/Casale"),
    MANSION("Palazzotto/Intero stabile"),
    BOX("Box/Garage/Posto auto"),
    COMMERCIAL("Locale commerciale"),
    INDUSTRIAL("Industriale/Artigianale"),
    OFFICE("Ufficio"),
    WAREHOUSE("Magazzino/Deposito"),
    BUILDING_PLOT("Terreno edificabile"),
    FARMLAND("Terreno agricolo");

}