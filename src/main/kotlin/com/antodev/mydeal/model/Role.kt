package com.antodev.mydeal.model

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id

@Entity
class Role {
    @Id
    @GeneratedValue
    var id: Long = 0

    @Column(unique = true, nullable = false)
    lateinit var name: String
}