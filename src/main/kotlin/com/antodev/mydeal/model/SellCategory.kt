package com.antodev.mydeal.model

enum class SellCategory(val description: String) {
    AUCTION("In Asta Giudiziaria"),
    SELL("In Vendita"),
    RENT("In Locazione/Affitto"),
    HOLYDAY("Casa Vacanze")
}