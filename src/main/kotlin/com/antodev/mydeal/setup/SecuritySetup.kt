package com.antodev.mydeal.setup

import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.web.authentication.LoginUrlAuthenticationEntryPoint
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.security.authentication.dao.DaoAuthenticationProvider



@Configuration
@EnableWebSecurity
class SecuritySetup : WebSecurityConfigurerAdapter(true) {
    @Autowired
    lateinit var userDetailsService: MyDealUserDetailService

    override fun configure(http: HttpSecurity?) {
        http!!.csrf().disable().
                exceptionHandling().authenticationEntryPoint(LoginUrlAuthenticationEntryPoint("/login")).accessDeniedPage("/accessDenied")
                .and().authorizeRequests()
                .antMatchers("/VAADIN/**", "/PUSH/**", "/UIDL/**", "/login", "/login/**", "/error/**", "/accessDenied/**", "/vaadinServlet/**").permitAll()
                .antMatchers("/admin").fullyAuthenticated().and()
                .antMatcher("/**").anonymous()
    }

    @Bean
    fun createDaoAuthenticationProvider(): DaoAuthenticationProvider {
        val provider = DaoAuthenticationProvider()

        provider.setUserDetailsService(userDetailsService)
        provider.setPasswordEncoder(passwordEncoder())
        return provider
    }

    @Bean
    fun passwordEncoder(): BCryptPasswordEncoder {
        return BCryptPasswordEncoder()
    }
}